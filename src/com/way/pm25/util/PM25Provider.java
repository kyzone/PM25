package com.way.pm25.util;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.way.pm25.API;

public class PM25Provider {
	private static final String LTag = "pm25";

	public void request(final PM25Info pm25Listener, String param) {
		GetTask task = new GetTask(API.PM25_API) {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				pm25Listener.onPreExecute();
			}
			protected void onPostExecute(String result) {
				ArrayList<PM25> pm25List = null;
				if (result != null && !result.contains("error")) {
					L.d(PM25Provider.LTag, result);
					try {
						JSONArray jsobArray = new JSONArray(result);
						pm25List = new ArrayList<PM25>();
						for (int i = 0; i < jsobArray.length(); i++) {
							JSONObject jsobObject = jsobArray
									.getJSONObject(i);
							PM25Provider.PM25 pm25 = new PM25Provider.PM25();
							pm25.aqi = jsobObject.optString("aqi");
							pm25.area = jsobObject.optString("area");
							pm25.pm2_5 = jsobObject.optString("pm2_5");
							pm25.pm10 = jsobObject.optString("pm10");
							pm25.position_name = jsobObject
									.optString("position_name");
							pm25.primary_pollutant = jsobObject
									.optString("primary_pollutant");
							pm25.quality = jsobObject.optString("quality");
							pm25.time_point = jsobObject
									.optString("time_point");
							pm25List.add(pm25);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					pm25Listener.onInfo(pm25List);
				}
			}
		};
		task.execute(new String[] { param.toLowerCase() });
	}

	public static class PM25 {
		public String aqi;
		public String area;
		public String pm10;
		public String pm2_5;
		public String position_name;
		public String primary_pollutant;
		public String quality;
		public String time_point;

	}

	public static abstract interface PM25Info {
		public abstract void onPreExecute();
		public abstract void onInfo(List<PM25Provider.PM25> pm25List);
	}
}
