package com.way.pm25;

import android.content.Context;
import android.content.SharedPreferences;

import com.way.pm25.util.L;

public class PM25CitySetting {
	private static final String LTag = "pm25";
	private SharedPreferences mPreferences;

	public PM25CitySetting(Context context) {
		mPreferences = context.getSharedPreferences("pm25_city_setting",
				Context.MODE_PRIVATE);
	}

	public String getAutoCity() {
		return mPreferences.getString("auto_city", "");
	}

	public String getCity() {
		return mPreferences.getString("setting_city", "auto");
	}

	public void setAutoCity(String autoCity) {
		L.d(LTag, String.format("setting auto city : %s ", autoCity));
		mPreferences.edit().putString("auto_city", autoCity).commit();
	}

	public void setCity(String city) {
		L.d(LTag, String.format("setting city : %s ", city));
		mPreferences.edit().putString("setting_city", city).commit();
	}
}